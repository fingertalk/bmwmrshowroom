﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControllerHandlerText : MonoBehaviour, ControllerHandlerInterface
{
    public GameObject OutputUI;
    public GameObject SuccessUI;

    AndroidJavaObject androidPlugin;
    
    // Use this for initialization
    void Start () {
        if (!Application.isEditor)
        {
            androidPlugin = new AndroidJavaObject("com.fingertalk.fingertalkcore.FingerTalkCore");
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnShow() { }
    public void OnHide() { }

    public void OnLongPress(int keycode)
    {
        Text output = OutputUI.GetComponent<Text>();
        if (keycode == 12)
        {
            output.text = "";
            SuccessUI.SetActive(true);
        }
    }

    public void OnSelect(int keycode)
    {
        TextInput(keycode);
    }

    private void TextInput(int keycode)
    {
        Debug.Log("[ControllerHandlerText] Send to core : " + keycode);
        SuccessUI.SetActive(false);
        Text output = OutputUI.GetComponent<Text>();
        output.text = androidPlugin.Call<string>("hangulInput", keycode, output.text.Length, output.text);
    }
}
