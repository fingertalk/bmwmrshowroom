﻿public interface BMWInterface
{
    void TurnRight();
    void TurnLeft();
    void ChangeExteriorColor(int option);
    void ChangeInteriorColor(int option);
    void ChangeWheel(int option);
}
