﻿using UnityEngine;
using System.Collections;

public class BMW5Series : MonoBehaviour, BMWInterface {

    public GameObject Container;

    // Exterior
    public Material BodyColor;

    private static Color ColorAlpineWhite = new Color(1.0f, 1.0f, 1.0f);
    private static Color ColorJetBlack = new Color(0.0f, 0.0f, 0.0f);
    private static Color ColorCashmereSilver = new Color(0.83f, 0.8f, 0.76f);
    private static Color ColorMediterraneanBlue = new Color(0.09f, 0.22f, 0.41f);
    private static Color ColorBluestoneMetallic = new Color(0.30f, 0.33f, 0.36f);

    private Color[] colors = {
        ColorJetBlack,
        ColorBluestoneMetallic,
        ColorMediterraneanBlue
    };

    // Use this for initialization
    void Start () {
        BodyColor.color = ColorJetBlack;
    }
	
	// Update is called once per frame
	void Update () {
        float rotateDegree;
        if (targetAngle != 0)
        {
            rotateDegree = Time.deltaTime * 50;
            Quaternion originalRot = Container.transform.rotation;
            if (targetAngle > 0)
            {
                Container.transform.rotation = originalRot * Quaternion.AngleAxis(rotateDegree, Vector3.up);
                targetAngle -= rotateDegree;
                if (targetAngle < 0) targetAngle = 0;
            }
            else if (targetAngle < 0)
            {
                Container.transform.rotation = originalRot * Quaternion.AngleAxis(-rotateDegree, Vector3.up);
                targetAngle += rotateDegree;
                if (targetAngle > 0) targetAngle = 0;
            }
        }
    }

    private float targetAngle = 0;

    public void TurnRight()
    {
        Debug.Log("[BMW 5 Series] TurnRight");
        targetAngle = 20;
    }

    public void TurnLeft()
    {
        Debug.Log("[BMW 5 Series] TurnLeft");
        targetAngle = -20;
    }

    public void ChangeExteriorColor(int option)
    {
        Debug.Log("[BMW 5 Series] ChangeExteriorColor");
        BodyColor.color = colors[option];
    }

    public void ChangeInteriorColor(int option)
    {
        Debug.Log("[BMW 5 Series] ChangeInteriorColor");
    }

    public void ChangeWheel(int option)
    {
        Debug.Log("[BMW 5 Series] ChangeWheel");
    }
}
