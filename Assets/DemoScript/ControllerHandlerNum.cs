﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControllerHandlerNum : MonoBehaviour, ControllerHandlerInterface {

    public GameObject OutputUI;
    public GameObject SuccessUI;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnShow() { }
    public void OnHide() { }

    public void OnLongPress(int keycode)
    {
        Text output = OutputUI.GetComponent<Text>();
        if (keycode == 12)
        {
            output.text = "";
            SuccessUI.SetActive(true);
        }
    }

    public void OnSelect(int keycode)
    {
        Debug.Log("[ControllerHandlerNum] OnSelect : " + keycode);
        SuccessUI.SetActive(false);
        Text output = OutputUI.GetComponent<Text>();
        switch (keycode)
        {
            case 11:
                output.text = output.text.Substring(0, output.text.Length - 1);
                break;
            case 12:
                output.text = output.text + "-";
                break;
            default:
                output.text = output.text + keycode;
                break;
        }
    }
}
