﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BMWi8 : MonoBehaviour, BMWInterface {

    public GameObject Container;

    public Material ExteriorColor1;
    public Material ExteriorColor2;

    public Material[] InteriorColors;
    public Material[] OptionColors;

    public GameObject[] Wheels;

    public GameObject ExteriorText;
    public GameObject InteriorText;
    public GameObject WheelText;

    private string[] ExteriorLabels =
    {
        "Crystal White Pearl Metallic",
        "Sophisto Gray Metallic",
        "Ionic Silver Metallic (BMW i Frozen Blue accent only)"
    };

    private string[] InteriorLabels =
    {
        "Mega Carum Spice Gray Leather with Cloth highligh",
        "Giga Ivory White Full Perforated Leather",
        "Tera Exclusive Dalbergia Brown Full Natural Leather with Carum Spice Gray Cloth"
    };

    private string[] WheelLabels =
    {
        "W Spoke 20\" – Style 470 with mixed tires",
        "Turbine 20\" – Style 625 with mixed tires",
        "M Double Spoke 20\" – Style 303M"
    };

    private static Color ColorAlpineWhite = new Color(1.0f, 1.0f, 1.0f);
    private static Color ColorJetBlack = new Color(0.0f, 0.0f, 0.0f);
    private static Color ColorCashmereSilver = new Color(0.6f, 0.6f, 0.6f);

    Animator AnimDoor;

    private Color[] colors = {
        ColorAlpineWhite,
        ColorJetBlack,
        ColorCashmereSilver
    };

    // Use this for initialization
    void Start () {
        AnimDoor = this.gameObject.GetComponent<Animator>();
        ExteriorColor1.color = ColorAlpineWhite;
    }
	
	// Update is called once per frame
	void Update () {
        float rotateDegree;
        if (targetAngle != 0) {
            rotateDegree = Time.deltaTime * 50;
            Quaternion originalRot = Container.transform.rotation;
            if (targetAngle > 0)
            {
                Container.transform.rotation = originalRot * Quaternion.AngleAxis(rotateDegree, Vector3.up);
                targetAngle -= rotateDegree;
                if (targetAngle < 0) targetAngle = 0;
            }
            else if (targetAngle < 0)
            {
                Container.transform.rotation = originalRot * Quaternion.AngleAxis(-rotateDegree, Vector3.up);
                targetAngle += rotateDegree;
                if (targetAngle > 0) targetAngle = 0;
            }
        }
    }

    private float targetAngle = 0;

    public void TurnRight()
    {
        Debug.Log("[BMW i8] TurnRight");
        targetAngle = 20;
    }

    public void TurnLeft()
    {
        Debug.Log("[BMW i8] TurnLeft");
        targetAngle = -20;
    }

    public void ChangeExteriorColor(int option)
    {
        Debug.Log("[BMW i8] ChangeExteriorColor");
        ExteriorColor1.color = colors[option];
        ExteriorText.GetComponent<Text>().text = ExteriorLabels[option];
        AnimDoor.SetBool("isInteriorMode", false);
    }

    public void ChangeInteriorColor(int option)
    {
        Debug.Log("[BMW i8] ChangeInteriorColor");
        for (int i=0; i < InteriorColors.Length; i++)
        {
            InteriorColors[i].color = OptionColors[option].color;
        }
        InteriorText.GetComponent<Text>().text = InteriorLabels[option];
        AnimDoor.SetBool("isInteriorMode", true);
    } 

    public void ChangeWheel(int option)
    {
        Debug.Log("[BMW i8] ChangeWheel");
        for (int i = 0; i < Wheels.Length; i++)
        {
            if (i != option)
            {
                Wheels[i].SetActive(false);
            } else
            {
                Wheels[i].SetActive(true);
            }
        }
        WheelText.GetComponent<Text>().text = WheelLabels[option];
        AnimDoor.SetBool("isInteriorMode", false);
    }


}
