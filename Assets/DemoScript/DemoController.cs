﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DemoController : MonoBehaviour {

    public GameObject[] Controllers;

    private TransitionManager mTransitionManager;
    private ControllerHandlerInterface currentController;
    private Dictionary<KeyCode, int> keyValueMapper = new Dictionary<KeyCode, int>();

    private bool isVR = false;

    // Long Press Control
    private bool isKeyDown = false;
    private float keydownTimestamp;
    private KeyCode keydownBuffer = KeyCode.None;

    // Use this for initialization
    void Start () {
        mTransitionManager = FindObjectOfType<TransitionManager>();
        initializeKeyValueMapper();
    }

    #region Key-Value Mapper
    private void initializeKeyValueMapper()
    {
        /* FingerTalk */
        keyValueMapper.Add(KeyCode.Alpha1, 1);
        keyValueMapper.Add(KeyCode.Alpha2, 2);
        keyValueMapper.Add(KeyCode.Alpha3, 3);
        keyValueMapper.Add(KeyCode.Alpha4, 4);
        keyValueMapper.Add(KeyCode.Alpha5, 5);
        keyValueMapper.Add(KeyCode.Alpha6, 6);
        keyValueMapper.Add(KeyCode.Alpha7, 7);
        keyValueMapper.Add(KeyCode.Alpha8, 8);
        keyValueMapper.Add(KeyCode.Alpha9, 9);
        keyValueMapper.Add(KeyCode.F1, 11);
        keyValueMapper.Add(KeyCode.Alpha0, 0);
        keyValueMapper.Add(KeyCode.F2, 12);
        /* Keyboard */
        keyValueMapper.Add(KeyCode.Q, 4);
        keyValueMapper.Add(KeyCode.W, 5);
        keyValueMapper.Add(KeyCode.E, 6);
        keyValueMapper.Add(KeyCode.A, 7);
        keyValueMapper.Add(KeyCode.S, 8);
        keyValueMapper.Add(KeyCode.D, 9);
        keyValueMapper.Add(KeyCode.Z, 11);
        keyValueMapper.Add(KeyCode.X, 0);
        keyValueMapper.Add(KeyCode.C, 12);
    }
    #endregion

    // Update is called once per frame
    void Update () {
        if (currentController != null) {
            handleKeyDown();
            handleKeyUp();
            handleLongPress();
        }
    }

    private void handleKeyDown()
    {
        foreach (KeyValuePair<KeyCode, int> entry in keyValueMapper)
        {
            if (Input.GetKeyDown(entry.Key))
            {
                currentController.OnSelect(entry.Value);
                setKeyDown(entry.Key);
            }
        }
    }

    private void handleKeyUp()
    {
        foreach (KeyValuePair<KeyCode, int> entry in keyValueMapper)
        {
            if (Input.GetKeyUp(entry.Key))
                releaseKeyDown();
        }
    }

    private void handleLongPress()
    {
        int key;
        if (isKeyDown && (Time.time - keydownTimestamp) > 1.0f)
        {
            Debug.Log("Long Press - " + keydownBuffer);
            if (currentController != null)
            {
                keyValueMapper.TryGetValue(keydownBuffer, out key);
                currentController.OnLongPress(key);
            }
        }
    }

    private void setKeyDown(KeyCode key)
    {
        isKeyDown = true;
        keydownTimestamp = Time.time;
        keydownBuffer = key;
    }

    private void releaseKeyDown()
    {
        isKeyDown = false;
        keydownTimestamp = Time.time;
        keydownBuffer = KeyCode.None;
    } 

    public void SetControlMode(GameObject target)
    {
        Debug.Log("[DemoController] SetControlMode : " + target.name);
        if (currentController != null) { 
            currentController.OnHide();
        }
        for (int i=0;i<Controllers.Length;i++)
        {
            if (Controllers[i].name.Equals(target.name))
            {
                Controllers[i].SetActive(true);
                
            } else
            {
                Controllers[i].SetActive(false);
            }
        }
        currentController = target.GetComponent<ControllerHandlerInterface>();
        if (currentController != null)
        {
            currentController.OnShow();
        }
    }

    private void toggleVR()
    {
        if (isVR)
        {
            mTransitionManager.Play(true);
            isVR = false;
        }
        else
        {
            mTransitionManager.Play(false);
            isVR = true;
        }
    }
}
