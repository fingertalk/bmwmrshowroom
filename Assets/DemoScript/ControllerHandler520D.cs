﻿using UnityEngine;
using System.Collections;

public class ControllerHandler520D : MonoBehaviour, ControllerHandlerInterface
{
    public GameObject model;

    private TransitionManager mTransitionManager;
    private BMWInterface modelController;
    private bool isVR = false;

    // Use this for initialization
    void Start()
    {
        mTransitionManager = FindObjectOfType<TransitionManager>();
        modelController = model.GetComponent<BMWInterface>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnShow() { }
    public void OnHide() { }

    public void OnLongPress(int keycode)
    {

    }

    public void OnSelect(int keycode)
    {
        switch (keycode)
        {
            case 1:
                modelController.ChangeExteriorColor(0);
                break;
            case 2:
                modelController.ChangeExteriorColor(1);
                break;
            case 3:
                modelController.ChangeExteriorColor(2);
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
            case 8:
                break;
            case 9:
                break;
            case 11:
                modelController.TurnLeft();
                break;
            case 0:
                modelController.TurnRight();
                break;
            case 12:
                toggleVR();
                break;
        }
    }

    private void toggleVR()
    {
        if (isVR)
        {
            mTransitionManager.Play(true);
            isVR = false;
        }
        else
        {
            mTransitionManager.Play(false);
            isVR = true;
        }
    }
}
