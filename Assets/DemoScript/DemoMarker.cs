﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class DemoMarker : MonoBehaviour, ITrackableEventHandler
{
    private DemoStage demoStage;
    private DemoController demoController;
    private TrackableBehaviour mTrackableBehaviour;

    public GameObject TargetController;
    public GameObject TargetCar;
    public GameObject UICanvas;

    // Use this for initialization
    void Start() {
        
        demoStage = FindObjectOfType<DemoStage>();
        demoController = FindObjectOfType<DemoController>();

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            if (UICanvas != null) { 
                UICanvas.SetActive(true);
            }
            //OnTrackingFound();
            if (TargetCar != null) { 
                demoStage.SetTarget(TargetCar);
            }
            if (TargetController != null)
            {
                demoController.SetControlMode(TargetController);
            }
        }
        else
        {
            //OnTrackingLost();
            if (UICanvas != null)
            {
                UICanvas.SetActive(false);
            }
        }
    }
}