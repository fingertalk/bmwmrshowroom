﻿public interface ControllerHandlerInterface
{
    void OnSelect(int key);
    void OnLongPress(int key);
    void OnShow();
    void OnHide();
}